# -*- coding:utf-8 -*-
# author: zby
# email: by951118@163.com
# date: 2021/7/1
import requests
# 停止爬虫 POST
url = "http://localhost:6800/cancel.json?project=PH&job=1647360425"
# 获取项目列表
url2 = "http://127.0.0.1:6800/listprojects.json"
# 获取爬虫的运行状态
url3 = "http://127.0.0.1:6800/listversions.json?project=PH"

# 启动服务器上某一爬虫（必须是已发布到服务器的爬虫） （post方式，data={“project”:myproject,“spider”:myspider}）
data={"project":"PH","spider":"top_spider"}
url4 = "http://127.0.0.1:6800/schedule.json"
resp = requests.post(url4,data=data)
print(resp.json())
resp = requests.get(url3)
print(resp.json())
