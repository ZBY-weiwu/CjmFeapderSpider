# -*- coding: utf-8 -*-
"""
Created on 2022-03-15 20:42:13
---------
@summary: 爬虫入口
---------
@author: NING MEI
"""

from feapder import ArgumentParser

from spiders import *

def crawl_xxx():
    """
    AirSpider爬虫
    """
    spider = list_spider.ListSpider()
    spider.start()


if __name__ == "__main__":
    crawl_xxx()
