# -*- coding:utf-8 -*-
# author: zby
# email: by951118@163.com
# date: 2021/7/1


import scrapy
from scrapy.crawler import CrawlerProcess

from paihang.spiders.tophub_today import TophubTodaySpider

# 创建一个CrawlerProcess对象
process = CrawlerProcess() # 括号中可以添加参数

process.crawl(TophubTodaySpider)
process.start()