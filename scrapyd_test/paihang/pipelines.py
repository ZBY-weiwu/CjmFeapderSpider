# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import json
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from paihang.items import PaihangItem

class PaihangPipeline:
    def process_item(self, item, spider):
        if isinstance(item, PaihangItem):
            d={}
            d["author"] =item["author"]
            d["url"] =item["url"]
            d["title"] =item["title"]
            print("title:",d["title"].encode())
            d["like_count"] =item["like_count"]
            d["collection_count"] =item["collection_count"]
            d["comment_count"] =item["comment_count"]
            with open("./output/data.json", "a+", encoding="utf-8") as f:
                f.write(json.dumps(d, ensure_ascii=False) + "\n")
                # f.write(json.dumps(d) + "\n")
