# -*- coding:utf-8 -*-
import scrapy
import re
import copy
from lxml import etree
import json
from paihang.items import PaihangItem

class TophubTodaySpider(scrapy.Spider):
    name = 'top_spider'

    def __init__(self,**kwargs):
        self.headers = {
            'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36"}

    def start_requests(self):
        yield scrapy.Request("https://tophub.today/", headers=self.headers,callback=self.parse)

    def parse(self, response):
        html = response.body.decode('utf8')
        html_etree = etree.HTML(html)
        card_elements = html_etree.xpath('//div[@class="cc-cd"]')

        # 过滤出对应的卡片元素【什么值得买】
        buy_good_element = [card_element for card_element in card_elements if
                            card_element.xpath('.//div[@class="cc-cd-is"]//span/text()')[0] == '什么值得买'][0]

        # 获取内部文章标题及地址
        a_elements = buy_good_element.xpath('.//div[@class="cc-cd-cb nano"]//a')

        for a_element in a_elements:
            # 标题和链接
            title = a_element.xpath('.//span[@class="t"]/text()')[0]
            # title.encode('utf-8', 'ignore').decode('gbk', 'ignore')
            # print("title:",title)
            href = a_element.xpath('.//@href')[0]
            list_item = {}
            list_item["title"]  = title
            list_item["url"]  = href

            # 再次下发新任务，并带上文章标题
            request = scrapy.Request(href, headers=self.headers,callback=self.parser_detail_page)
            request.meta["list_item"] = copy.deepcopy(list_item)
            yield request


    def parser_detail_page(self, response):
        """
        解析文章详情数据
        :param request:
        :param response:
        :return:
        """
        html = response.body.decode('utf8')
        html_etree = etree.HTML(html)
        item = PaihangItem()
        list_item = response.meta.get("list_item")
        title = list_item["title"]
        url = response.url

        # 解析文章详情页面，获取点赞、收藏、评论数目及作者名称
        author = html_etree.xpath('//a[@class="author-title"]/text()')[0].strip()
        item["author"] = author
        item["url"] = response.url
        item["title"] = title
        # print("作者：", author, '文章标题:', title, "地址：", url)

        desc_elements = html_etree.xpath('//span[@class="xilie"]/span')

        # print("desc数目:", len(desc_elements))

        # 点赞
        like_count = int(re.findall('\d+', desc_elements[1].xpath('./text()')[0])[0])
        # 收藏
        collection_count = int(re.findall('\d+', desc_elements[2].xpath('./text()')[0])[0])
        # 评论
        comment_count = int(re.findall('\d+', desc_elements[3].xpath('./text()')[0])[0])
        item["like_count"] = like_count
        item["collection_count"] = collection_count
        item["comment_count"] = comment_count
        # print("点赞：", like_count, "收藏:", collection_count, "评论:", comment_count)

        yield item
