# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class PaihangItem(scrapy.Item):
    # define the fields for your item here like:
    author = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    like_count = scrapy.Field()
    collection_count = scrapy.Field()
    comment_count = scrapy.Field()
